$(document).ready(function () {
  $(".datepicker").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years",
    endDate: new Date(),
    toggleActive: true,
    orientation: "bottom auto",
    autoclose: true, //to close picker once year is selected
  });

  $("#marques").select;

  marques.forEach((marque) => {
    $("#marques").append($("<option>").text(marque).attr("value", marque));
  });

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var formInputs = document.querySelectorAll(".needs-validation");

  $("#form").submit((event) => {
    event.preventDefault();
    let validators = [];
    Array.prototype.slice.call(formInputs).forEach((input) => {
      checkFormValidity(input, event, validators);
    });
    if (validators.every((v) => v == true)) {
      submitDemande().then(() => onSuccess());
    }
  });
});

function checkFormValidity(input, event, validators) {
  let prix = $("#prix").get(0);
  if (prix.value == undefined || prix.value == "") {
    $("#invalid-feedback-prix").text("Veuillez renseigner un prix");
  } else if (!prix.value.match(prix.pattern)) {
    $("#invalid-feedback-prix").text(
      "Le prix ne doit contenir que des chiffres"
    );
  } else if (prix.value < 0 || prix.value > 1000000) {
    $("#invalid-feedback-prix").text(
      "Le prix doit être entre 0 € et 1 000 000 €"
    );
  }

  if (!input.checkValidity()) {
    event.preventDefault();
    event.stopPropagation();
    validators.push(false);
  }

  input.classList.add("was-validated");
  validators.push(true);
}

async function submitDemande() {
  const mail = $("#mail").val();
  const marque = $("#marques").val();
  const modele = $("#modele").val();
  const annee = $("#annee").val();
  const kilometrage = $("#kilometrage").val();
  const prix = $("#prix").val();
  const message = $("#message").val();

  const template = {
    from: mail,
    marque: marque,
    modele: modele,
    annee: annee,
    kilometrage: kilometrage,
    prix: prix,
    message: message,
  };

  emailjs.init("user_W3AFP2shDIAiHW0MFhaPv");

  emailjs.send("service_camping_car", "template_camping_car", template);
}

function onSuccess() {
  const form = $("#form").get(0);
  $(".toast").toast("show");
  setTimeout(() => {
    form.classList.remove("was-validated");
    form.reset();
  }, 250);
}

var marques = [
  "ADRIA",
  "ALCAR",
  "ARCA",
  "AUTOSTAR",
  "AUTOROLLER ROLLERTEAM",
  "BAVARIA CAMP",
  "BENIMAR",
  "BLUCAMP",
  "BRAVIA",
  "BURSTNER",
  "CAMPEREVE",
  "CAR CARAVANE",
  "CARADO",
  "CARAVANS INTERNATIONAL",
  "CARTHAGO",
  "CHALLENGER",
  "CHAUSSON",
  "CI",
  "CONCORDE",
  "CSA GERARD",
  "DETHLEFFS",
  "DINGHY",
  "ELNAGH",
  "ERIBA",
  "ESTEREL",
  "EURA MOBIL",
  "EVASION 24",
  "FENDT",
  "FLEURETTE",
  "FLORIUM",
  "FONT VENDOME",
  "FRANKIA",
  "GIOTTILINE",
  "GLENAN",
  "GLOBECAR",
  "HOBBY",
  "HYMER MOBIL",
  "ILUSION",
  "ITINEO",
  "JCG",
  "KARMAN-MOBIL",
  "KENTUCKY",
  "KNAUS",
  "LA STRADA",
  "LAIKA",
  "LANDO",
  "LEVOYAGEUR",
  "LOISIR 12",
  "MCC",
  "MCLOUIS",
  "MERCEDES",
  "MOBILVETTA",
  "MOOVEO",
  "MORELO",
  "NIESMANN&BISCHOFF",
  "NOTIN",
  "OXYGENE",
  "PHOENIX",
  "PILOTE",
  "PLA",
  "POSSL",
  "RAPIDO",
  "RIMOR",
  "ROLLER TEAM",
  "SAVIGNAT ROCHE",
  "STYLEVAN",
  "SUN LIVING",
  "SUNLIGNT",
  "TEC",
  "VOLKSWAGEN",
  "WESTFALIA",
  "WEINSBERG",
  "WINNEBAGO",
  "WINGAMM",
];
